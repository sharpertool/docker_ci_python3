FROM sharpertool/circleci_python:3.6.1

RUN sudo apt-get install awscli rsync

RUN sudo /bin/bash -c 'sudo curl -L https://github.com/docker/compose/releases/download/1.17.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose'
RUN sudo chmod +x /usr/local/bin/docker-compose

RUN sudo pip install -U awscli

RUN curl -sL https://sentry.io/get-cli/ | bash

ADD build_container.bashrc /home/circleci/.bashrc
